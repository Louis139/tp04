﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Dinosaur Louis = new Dinosaur("Louis", "Stegausaurus", 18);

            Assert.Equal("Louis", louis.name);
            Assert.Equal("Stegausaurus", louis.specie);
            Assert.Equal(18, louis.age);
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            Dinosaur Louis = new Dinosaur("Louis", "Stegausaurus", 18);
            Assert.Equal("Rrrr", louis.roar());
        }

        [TestMethod]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 18);
            Assert.Equal("Je m'appelle Louis le Stegausaurus, j'ai 18 ans.", louis.sayHello());
        }
    }
    }
}
